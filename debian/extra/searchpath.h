#ifndef _HCS_SEARCHPATH_H
#define _HCS_SEARCHPATH_H
#include <dlfcn.h>
#if PD_MAJOR_VERSION==0 && PD_MINOR_VERSION<48
struct _instancestuff
{
  t_namelist *st_externlist;
  t_namelist *st_searchpath;
  t_namelist *st_staticpath;
  t_namelist *st_helppath;
  /* ... */
};
typedef struct _pdinstance
{
  double pd_systime;          /* SKIP */
  t_clock *pd_clock_setlist;  /* SKIP */
  t_canvas *pd_canvaslist;    /* SKIP */
  int pd_instanceno;          /* SKIP */
  t_symbol **pd_symhash;      /* SKIP */
  void *pd_midi;              /* SKIP */
  void *pd_inter;             /* SKIP */
  void *pd_ugen;              /* SKIP */
  void *pd_gui;               /* SKIP */
  struct _instancestuff *pd_stuff;  /* semi-private stuff in s_stuff.h */
  /* ... */
} t_hcs_pdinstance;
# ifndef t_pdinstance
#  define t_pdinstance t_hcs_pdinstance;
# endif
#endif
#endif /* _HCS_SEARCHPATH_H */
